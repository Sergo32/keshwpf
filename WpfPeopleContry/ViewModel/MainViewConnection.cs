﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using WpfPeopleContry.Model;
using WpfPeopleContry.Model.Table;
using WpfPeopleContry.Model.Entities;


namespace WpfPeopleContry.ViewModel
{
    class MainViewConnection : INotifyPropertyChanged
    {

        public ObservableCollection<EntitiesPeople> TablePeopleCollection
        {
            get { return dbManager.TablePeople.Rows; }
        }
        public ObservableCollection<EntitiesContry> TableContryCollection
        {
            get { return dbManager.TableContry.Rows; }
        }
        private   DbManager dbManager;
        public MainViewConnection()
        {
            dbManager = DbManager.GetInstance();
            insertPeople = new EntitiesPeople();
            insertContry = new EntitiesContry();

        }
        private EntitiesPeople insertPeople;// ячейка адрес памяти
        public EntitiesPeople InsertPeople//свойсва управл перемен
        {
            get { return insertPeople; }
            set { insertPeople = value; OnPropertyChanged("InsertPeople"); }
        }
        private EntitiesContry insertContry;// ячейка адрес памяти
        public EntitiesContry InsertContry//свойсва управл перемен
        {
            get { return insertContry; }
            set { insertContry = value; OnPropertyChanged("InsertContry"); }
        }

        private RelayCommand relayAddPeople;
        public RelayCommand RelayCommandPeopleAdd
        {
            get
            {
                return relayAddPeople ?? (relayAddPeople = new RelayCommand(
               (obj) =>
               {
                   dbManager.TablePeople.AddPeople(InsertPeople);
                   OnPropertyChanged("TablePeopleCollection");
               })
                   );
            }
        }

        private RelayCommand relayDeletePeople;
        public RelayCommand RelayCommandPeopleDelete
        {
            get
            {
                return relayDeletePeople ?? (relayDeletePeople = new RelayCommand(
               (obj) =>
               {
                   dbManager.TablePeople.Delete(InsertPeople.Id);
                   OnPropertyChanged("TablePeopleCollection");
               })
                   );
            }
        }

        private RelayCommand relayUpdatePeople;
        public RelayCommand RelayCommandPeopleUpdate
        {
            get
            {
                return relayUpdatePeople ?? (relayUpdatePeople = new RelayCommand(
               (obj) =>
               {
                   dbManager.TablePeople.UpdatePeople(InsertPeople);
                   OnPropertyChanged("TablePeopleCollection");
               })
                   );
            }
        }

        private RelayCommand relayAddContry;
        public RelayCommand RelayCommandContryAdd
        {
            get
            {
                return relayAddContry ?? (relayAddContry = new RelayCommand(
               (obj) =>
               {
                   dbManager.TableContry.AddContry(InsertContry);
                   OnPropertyChanged("TableContryCollection");
               })
                   );
            }
        }
        private RelayCommand relayDeleteContry;
        public RelayCommand RelayCommandContryDelete
        {
            get
            {
                return relayDeleteContry ?? (relayDeleteContry = new RelayCommand(
               (obj) =>
               {
                   dbManager.TableContry.DeleteContry(InsertContry.Id);
                   OnPropertyChanged("TableContryCollection");
               })
                   );
            }
        }
        private RelayCommand relayUpdateContry;
        public RelayCommand RelayCommandContryUpdate
        {
            get
            {
                return relayUpdateContry ?? (relayUpdateContry = new RelayCommand(
               (obj) =>
               {
                   dbManager.TableContry.UpdateContry(InsertContry);
                   OnPropertyChanged("TableContryCollection");
               })
                   );
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
