﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using WpfPeopleContry.Model.Entities;
using MySql.Data.MySqlClient;
using WpfPeopleContry.Model.Tols;

namespace WpfPeopleContry.Model.Table
{
    class TablePeople
    {
        public ObservableCollection<EntitiesPeople> Rows { get; set; }
        private MySqlCommand sqlCommand;       
        public TablePeople(TableContry tableContry)
        {
            Rows = new ObservableCollection<EntitiesPeople>();
            sqlCommand = DbConnection.GetInstance().GetCommand();
            LoadRows(tableContry);


        }
        public void LoadRows(TableContry tableContry)
        {
            sqlCommand.CommandText = "CALL select_all_people()";
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                EntitiesPeople people = new EntitiesPeople()
                {

                    Id = reader.GetInt32("id"),
                    Name = reader.GetString("name"),
                    Id_contry = reader.GetInt32("id_country")
                };
                people.contries = tableContry.Rows.First(item => item.Id == people.Id_contry);

                Rows.Add(people);
            }
            reader.Close();

        }
        public void AddPeople(EntitiesPeople entitiesPeople)
        {
            sqlCommand.CommandText = $"CALL add_people('{entitiesPeople.Name}',{entitiesPeople.contries.Id})";
            sqlCommand.ExecuteNonQuery();

            sqlCommand.CommandText = "CALL get_lust_insert_id()";
            entitiesPeople.Id = Convert.ToInt32(sqlCommand.ExecuteScalar());
            entitiesPeople.Id_contry = entitiesPeople.contries.Id;

            Rows.Add(entitiesPeople);
        }
        public void Delete(int id)
        {
                sqlCommand.CommandText = $"CALL delete_people('{id}')";
                Rows.Remove(Rows.First(item => item.Id == id));
        }
        public void UpdatePeople(EntitiesPeople entitiesPeople)
        {

            entitiesPeople.Id_contry= entitiesPeople.contries.Id;
            sqlCommand.CommandText = $"CALL update_people({entitiesPeople.Id},'{entitiesPeople.Name}',{entitiesPeople.Id_contry})";
                sqlCommand.ExecuteNonQuery();

                int id = (int)sqlCommand.LastInsertedId;

                EntitiesPeople finded = Rows.First(item => item.Id == entitiesPeople.Id);
                finded.Name = entitiesPeople.Name;
                finded.Id_contry = entitiesPeople.Id_contry;
                finded.contries = entitiesPeople.contries;

        }

    }
}
