﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using WpfPeopleContry.Model.Entities;
using MySql.Data.MySqlClient;
using WpfPeopleContry.Model.Tols;

namespace WpfPeopleContry.Model.Table
{
    class TableContry
    {

        public ObservableCollection<EntitiesContry> Rows { get; set; }        
        private MySqlCommand sqlCommand;
    
        public TableContry()
        {
            Rows = new ObservableCollection<EntitiesContry>();
            sqlCommand = DbConnection.GetInstance().GetCommand();
            LoadRows();

        }
        public void LoadRows()
        {
            sqlCommand.CommandText = "CALL select_all_contry()";
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                Rows.Add(new EntitiesContry()
                {
                    Id = reader.GetInt32("id"),
                    Name = reader.GetString("name")
                });               
            }
            reader.Close();           
        }
        public void AddContry(EntitiesContry  entitiesContry)
        {
            sqlCommand.CommandText = $"CALL add_contry('{entitiesContry.Name}')";
            sqlCommand.ExecuteNonQuery();

            sqlCommand.CommandText = "CALL get_lust_insert_id()";
            entitiesContry.Id = Convert.ToInt32(sqlCommand.ExecuteScalar());

            Rows.Add(entitiesContry);
        }
        public void DeleteContry(int id)
        {
            sqlCommand.CommandText = $"CALL delete_contry('{id}')";
            Rows.Remove(Rows.First(item => item.Id == id));
        }
        public void UpdateContry(EntitiesContry entitiesContry)
        {
            sqlCommand.CommandText = $"CALL update_contry({entitiesContry.Id},'{entitiesContry.Name}')";
            int affectedRows = sqlCommand.ExecuteNonQuery();

            int id = (int)sqlCommand.LastInsertedId;

            EntitiesContry finded = Rows.First(item => item.Id == entitiesContry.Id);
            finded.Name = entitiesContry.Name;


        }

    }
}
