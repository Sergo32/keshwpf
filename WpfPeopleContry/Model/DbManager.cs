﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using WpfPeopleContry.Model.Table;
using WpfPeopleContry.Model.Entities;


namespace WpfPeopleContry.Model
{
    class DbManager
    {
       public TableContry TableContry { get; private set; }
    
        public TablePeople TablePeople { get; private set; }

        private static DbManager instance = null;

        public bool IsConnected { get; private set; }

        private DbManager()// private чтоб только 1 раз создать 
        {
            try
            {
                TableContry = new TableContry();
                TablePeople = new TablePeople(TableContry);            
                IsConnected = true;
            }
            catch 
            {
                IsConnected = false;
            }
                  
        }
        public static DbManager GetInstance()
        {
            if (instance==null)
            {
                instance = new DbManager();
            }
            return instance;
        }
    }
}
