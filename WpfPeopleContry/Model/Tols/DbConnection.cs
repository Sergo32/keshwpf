﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WpfPeopleContry.Model.Tols
{
    class DbConnection
    {

        private static DbConnection instance = null;
        private MySqlCommand sqlCommand;
        private MySqlConnection sqlConnection;

        public DbConnection()
        {
            string connection= "Server=127.0.0.1;UserId=root;Password=1234;Database=people_contry";
            sqlConnection = new MySqlConnection(connection);
            sqlConnection.Open();

            sqlCommand = new MySqlCommand();
            sqlCommand.Connection = sqlConnection;
        }

        ~DbConnection()
        {
            sqlConnection.Close();
        }

        public MySqlCommand GetCommand()
        {
            return sqlCommand;
        }

        public static DbConnection GetInstance()
        {
            if (instance==null)
            {
                instance = new DbConnection();
            }
            return instance;
        }

    }
}
